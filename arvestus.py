#!/usr/bin/python
# -*- coding: utf-8 -*-
#Marko Mandel
#
#Skript teeb k2surea parameeteriga antud serveri pihta HTTP GET p2ringuid, mis on teise #argumendina antud failis ja otsib vastusena saadud lehelt samas failis antud otsitavat stringi.
#
#Kui string leitakse siis kirjutatakse v2ljundisse OK ja kui ei leita, siis NOK
#
#(v2ga suur osa skriptist on v6etud minu skriptist otsi.py)

import sys
import os
import urllib
import time
import datetime

#kontrollib kas argumente on 2
if len(sys.argv) != 3:
    print 'Kasutamine: arvestus.py <serveri nimi v6i ip-aadress> <v2ljund>'
    sys.exit()

aadress = sys.argv[1]
sisendfail = os.path.abspath(sys.argv[2])

#kui url ei hakka s6nega 'http', siis pannakse "http://" urli ette, ja proovitakse uuesti
if aadress[0][0:4] != 'http':
   aadress = "http://" + aadress


#kontrolli kas sisendfail on olemas
try:
    open(sisendfail)
except IOError:
    print 'Viga: Sisendfaili ei leitud!'
    sys.exit()
else:
    pass
    
#paneb sisendfaili 2D-massiivi
sisend = open(sisendfail)    
url = []
for line in sisend.readlines():
    line = line.strip()
    url.append(line.split(','))
sisend.close()


for saidinimi in url:
    #kontrolli kas veebileht on olemas
    try:
        saidinimi[1] 
        ajutineobjekt = urllib.urlopen(aadress + saidinimi[0])
    except NameError:
        print 'Viga: ' + aadress + saidinimi[0] + ' veebisaiti pole olemas v6i ei saa sinna yhendust luua!'
        sys.exit()
    except IOError:
        print 'Viga: ' + aadress + saidinimi[0] + ' veebisaiti pole olemas v6i ei saa sinna yhendust luua!'
        sys.exit()
    except IndexError:
        print 'Viga: ' + aadress + saidinimi[0] + ' veebisaidi taga pole otsis6na!'
        sys.exit()
    else:
        ajutineobjekt.close()

    #salvestab saidi html-koodi 'sait' muutujasse
    yhendus = urllib.urlopen(aadress + saidinimi[0])
    sait = yhendus.read() 
    yhendus.close() 
    
    #timestambi v6tmine ja sellest kuup2eva arvutamine
    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d_%H-%M-%S')
    
    #kirjutab tulemused v2ljundisse
    if saidinimi[1] in sait:
        print aadress + ',' + saidinimi[1] + ',' + st + ',OK'
    else:
        print aadress + ',' + saidinimi[1] + ',' + st + ',NOK'


